|                             | T1   | T2   | T3   | T4   | T5   | T6   |
|:----------------------------|:-----|:-----|:-----|:-----|:-----|:-----|
| Just Read the Instructions  | -    | -    | -    | -    | -    | -    |
| Alex135                     | F    | F    | P    | F    | P    | P    |
| simonli                     | -    | -    | -    | -    | -    | -    |
| john                        | F    | F    | P    | F    | P    | F    |
| President Roosevelt         | F    | F    | P    | P    | P    | P    |
| Carly Rae Jepsen            | -    | -    | -    | -    | -    | -    |
| dh                          | F    | F    | P    | F    | P    | P    |
| hydrogenation-in-progress   | F    | F    | P    | F    | P    | P    |
| TheBananaMan                | F    | F    | P    | F    | P    | P    |
| <Error />                   | F    | F    | P    | P    | P    | P    |
| LivOr                       | P    | P    | P    | F    | P    | P    |
| Robotzilla                  | F    | F    | P    | P    | P    | F    |
| br no topo                  | F    | F    | F    | F    | -    | -    |
| Willou                      | F    | -    | P    | P    | P    | P    |
| Eren                        | F    | F    | P    | P    | P    | P    |
| Pecorino Romano             | -    | -    | -    | -    | -    | -    |
| Leonardo                    | F    | F    | P    | P    | P    | P    |
| snapdragon                  | P    | P    | P    | P    | P    | P    |
| DCfan88                     | -    | -    | P    | P    | P    | P    |
| Ripley                      | F    | F    | P    | P    | P    | P    |
| zhengsong                   | F    | F    | P    | P    | P    | P    |
| OneTwoThree                 | -    | -    | -    | -    | -    | -    |
| The_Very_Hungry_Caterpillar | F    | F    | P    | P    | P    | P    |
| Marian                      | -    | -    | -    | -    | -    | -    |
| mhd                         | F    | F    | P    | P    | P    | P    |
| gabs                        | -    | -    | -    | -    | -    | -    |
| meep                        | P    | P    | P    | P    | P    | P    |
| pamplemousse                | F    | F    | F    | P    | P    | P    |
| Zeroth                      | F    | F    | F    | F    | P    | P    |
| jingyang                    | -    | -    | -    | -    | -    | -    |
| hwu                         | F    | F    | F    | F    | F    | F    |
| mmoumn1                     | F    | F    | P    | P    | P    | P    |
| Intrigue162                 | -    | -    | P    | P    | P    | P    |
| berserkfan123               | -    | -    | -    | -    | -    | -    |
| TOOMUCHDOG                  | F    | F    | P    | P    | P    | P    |
| davie                       | -    | -    | -    | -    | -    | -    |
| Kaddy                       | -    | -    | -    | -    | -    | -    |
| Leo                         | -    | -    | -    | -    | -    | -    |
| shnf_jr                     | F    | F    | P    | P    | P    | P    |
| Roujon                      | -    | -    | -    | -    | -    | -    |
| incognito_banana            | F    | F    | P    | P    | P    | P    |
| neohuang                    | F    | F    | F    | F    | P    | F    |
| pi                          | P    | P    | P    | P    | P    | P    |
| NDB                         | -    | -    | -    | -    | -    | -    |
| PM                          | F    | F    | P    | P    | P    | F    |
| Potato                      | P    | F    | P    | P    | P    | P    |
| daniel.li3                  | -    | -    | -    | -    | -    | -    |
| crazywarqy                  | F    | -    | P    | P    | P    | P    |
| nope                        | -    | -    | -    | -    | -    | -    |
| kiU                         | F    | F    | F    | F    | P    | P    |
| Alex33                      | F    | F    | F    | F    | P    | F    |
| Mike L                      | P    | F    | P    | F    | P    | P    |